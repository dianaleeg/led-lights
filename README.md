# LED-lights

This repository contains various .ino files meant to run on an Arduino. The ***AdaFruit NeoPixel Library*** is needed to compile each ino. 

_Adafruit NeoPixel Library_ can be found in the Arduino IDE: 
**Tools -> Manage Libraries -> "Adafruit Neopixel" -> Install**

These ino files can be run on any LED strip that the NeoPixel Library supports ( I specifically used WS2811). It is recommended to use a **1000uF capacitor** across the +/- terminals and to use a **500Ohm resistor** across the signal. 

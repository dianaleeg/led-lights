#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
  #include <avr/power.h>
#endif

#define PIN 7

Adafruit_NeoPixel strip = Adafruit_NeoPixel(50, PIN, NEO_GRB + NEO_KHZ800);


void setup() {

  strip.begin();
  strip.setBrightness(50);
  strip.show();
  
}

void loop() {

  uint16_t wait = 100;
  colorful_blink(wait); // Params: Red, Green, Delay 50ms

  for (int i = 0; i < 1; i++){
    multi_color_swing(strip.Color(0, 150, 0), strip.Color(150, 0, 0), strip.Color(0, 0, 150), strip.Color(150, 150, 0), wait/2); // Params: Red, Green, Delay 50ms
  }

  for (int i = 0; i < 1; i++){
    pingPong(strip.Color(0, 150, 0), strip.Color(150, 0, 0), wait/2); // Params: Red, Green, Delay 50ms
  }

  for (int i = 0; i < 5; i++){
    blinkingStrip(strip.Color(0, 150, 0), strip.Color(150, 0, 0), wait); // Params: Red, Green, Delay 50ms
  } 

  for (int i = 0; i < 5; i++){
    theaterChase(strip.Color(0, 150, 0), strip.Color(150, 0, 0), wait/4); // Params: Red, Green, Delay 50ms
  } 
}

void colorful_blink(uint8_t wait) {
  
  uint32_t firstPixelHue = 0;
  
  for (int j=0; j < 20; j = j+1){ 
    for (int i = 0; i < 50; i = i+1){
      int pixelHue = firstPixelHue + (i * 65536L / strip.numPixels());
      strip.setPixelColor(i, strip.gamma32(strip.ColorHSV(pixelHue)));
      }
      
    strip.show();
    delay(wait);
      
    for (int i = 0; i < 50; i = i+1){
      strip.setPixelColor(i, 0);
      }
    
    strip.show();
    delay(wait);
    
  }
  
  for (int i = 0; i < 50; i = i+1){
    int pixelHue = firstPixelHue + (i * 65536L / strip.numPixels());
    strip.setPixelColor(i, strip.gamma32(strip.ColorHSV(pixelHue)));
    strip.show();
    delay(wait/2);
    }

}

void multi_color_swing(uint32_t first_color, uint32_t second_color, uint32_t third_color, uint32_t fourth_color, uint8_t wait) {
  for (uint16_t i=0; i <= strip.numPixels()/2; i=i+1) {
      strip.setPixelColor(i, first_color);
      strip.show();
      delay(wait);
  }
    
  for (uint16_t j= strip.numPixels()/2; j <= strip.numPixels(); j=j+1) {
     strip.setPixelColor(j, second_color);
     strip.show();
     delay(wait);
   }

  for (uint16_t j= strip.numPixels(); j > strip.numPixels()/2 - 1; j=j-1) {
     strip.setPixelColor(j,third_color);
     strip.show();
     delay(wait);
   }

  for (uint16_t j= strip.numPixels()/2 - 1; j > 0; j=j-1) {
     strip.setPixelColor(j,fourth_color);
     strip.show();
     delay(wait);
   }

   strip.setPixelColor(0,fourth_color);
   strip.show();
   delay(wait);

}

void pingPong(uint32_t first_color, uint32_t second_color, uint8_t wait) {
  for (uint16_t i=0; i <= strip.numPixels(); i=i+1) {
      strip.setPixelColor(i, first_color);
      strip.show();
      delay(wait);
  }
    
  for (uint16_t j= strip.numPixels(); j > 0; j=j-1) {
     strip.setPixelColor(j, second_color);
     strip.show();
     delay(wait);
   }

   // Had to set the first pixel separately, otherwise the lights dont come back
   strip.setPixelColor(0, second_color);
   strip.show();
   delay(wait);
}

void blinkingStrip(uint32_t first_color, uint32_t second_color, uint8_t wait) {
  for (uint16_t i=0; i < strip.numPixels(); i=i+1) {
      strip.setPixelColor(i, first_color);
  }

    strip.show();
    delay(wait);
    
  for (uint16_t i=0; i < strip.numPixels(); i=i+1) {
     strip.setPixelColor(i, second_color);
   }
        strip.show();
     delay(wait);
}

void theaterChase(uint32_t first_color, uint32_t second_color, uint8_t wait) {
  for (uint16_t i=0; i < strip.numPixels(); i=i+1) {
    if (i%2 == 0){
      strip.setPixelColor(i, first_color);
    }
    else{
      strip.setPixelColor(i, second_color);
    }
    strip.show();
    delay(wait);

    }
  for (uint16_t i=0; i < strip.numPixels(); i=i+1) {
    if (i%2 == 0){
      strip.setPixelColor(i, second_color);
    }
    else{
      strip.setPixelColor(i, first_color);
    }
    strip.show();
    delay(wait);

    }
}

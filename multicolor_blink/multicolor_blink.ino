#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
  #include <avr/power.h>
#endif

#define PIN 7

Adafruit_NeoPixel strip = Adafruit_NeoPixel(50, PIN, NEO_GRB + NEO_KHZ800);


void setup() {

  strip.begin();
  strip.setBrightness(50);
  strip.show();
  
}

void loop() {

  uint16_t wait = 100;
  colorful_blink(wait); // Params: Red, Green, Delay 50ms

}

void colorful_blink(uint8_t wait) {
  
  uint32_t firstPixelHue = 0;
  
  for (int j=0; j < 20; j = j+1){ 
    for (int i = 0; i < 50; i = i+1){
      int pixelHue = firstPixelHue + (i * 65536L / strip.numPixels());
      strip.setPixelColor(i, strip.gamma32(strip.ColorHSV(pixelHue)));
      }
      
    strip.show();
    delay(wait);
      
    for (int i = 0; i < 50; i = i+1){
      strip.setPixelColor(i, 0);
      }
    
    strip.show();
    delay(wait);
    
  }
  
  for (int i = 0; i < 50; i = i+1){
    int pixelHue = firstPixelHue + (i * 65536L / strip.numPixels());
    strip.setPixelColor(i, strip.gamma32(strip.ColorHSV(pixelHue)));
    strip.show();
    delay(wait/2);
    }

}

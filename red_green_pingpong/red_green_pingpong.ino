#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
  #include <avr/power.h>
#endif

#define PIN 7

Adafruit_NeoPixel strip = Adafruit_NeoPixel(50, PIN, NEO_GRB + NEO_KHZ800);


void setup() {

  strip.begin();
  strip.setBrightness(50);
  strip.show();
  
}

void loop() {

  uint16_t wait = 50;
  pingPong(strip.Color(0, 150, 0), strip.Color(150, 0, 0), wait); // Params: Red, Green, Delay 50ms

}

void pingPong(uint32_t first_color, uint32_t second_color, uint8_t wait) {
  for (uint16_t i=0; i <= strip.numPixels(); i=i+1) {
      strip.setPixelColor(i, first_color);
      strip.show();
      delay(wait);
  }
    
  for (uint16_t j= strip.numPixels(); j > 0; j=j-1) {
     strip.setPixelColor(j, second_color);
     strip.show();
     delay(wait);
   }

   // Had to set the first pixel separately, otherwise the lights dont come back
   strip.setPixelColor(0, second_color);
   strip.show();
   delay(wait);
}
